    function previewProfilePicture(event) {
        const preview = document.getElementById('profile-picture-preview');
        const file = event.target.files[0];
        const reader = new FileReader();

        reader.onload = function () {
            preview.src = reader.result;
            preview.style.display = 'block';
        }

        if (file) {
            reader.readAsDataURL(file);
        } else {
            preview.src = '#';
        }
    }



// JavaScript code for handling profile editing form submission

$(document).ready(function() {
    $('#profile-form').submit(function(event) {
        event.preventDefault(); // Prevent the form from submitting normally

        // Get the form data
        var formData = new FormData(this);

        // AJAX request to handle form submission
        $.ajax({
            type: 'POST',
            url: 'handle_profile_editing.php', // Replace with your backend endpoint
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                // Handle success response
                alert('Profile updated successfully!');
            },
            error: function(xhr, status, error) {
                // Handle error response
                console.error(xhr.responseText);
                alert('An error occurred while updating the profile.');
            }
        });
    });
});
