document.addEventListener("DOMContentLoaded", () => {
    document.querySelector(".signInButton").addEventListener("click", showSignInForm);
    document.querySelector(".registerButton").addEventListener("click", showRegisterForm);

    document.getElementById("registerForm").addEventListener("submit", function(event) {
        event.preventDefault();
        signUp();
    });

    document.getElementById("loginForm").addEventListener("submit", function(event) {
        event.preventDefault();
        login();
    });
});

function showSignInForm() {
    document.querySelector(".signInForm").style.display = "block";
    document.querySelector(".registerForm").style.display = "none";
}

function showRegisterForm() {
    document.querySelector(".signInForm").style.display = "none";
    document.querySelector(".registerForm").style.display = "block";
}

function signUp() {
    var _data = {
        username: document.getElementById("Name").value,
        email: document.getElementById("email").value,
        pass: document.getElementById("pass").value,
        confirmPassword: document.getElementById("confirmPass").value
    };

    if (_data.pass !== _data.confirmPassword) {
        alert("PASSWORD doesn't match!");
        return;
    }

    fetch("/signup", {
        method: "POST",
        body: JSON.stringify({
            username: _data.username,
            email: _data.email,
            pass: _data.pass
        }),
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        }
    })
    .then(response => {
        if (response.status === 201) {
            window.location.href = "Signup.html";
        } else {
            return response.json().then(err => { throw new Error(err.message) });
        }
    })
    .catch(e => {
        alert("Error: " + e.message);
    });
}

// function login() {
//     var username = document.getElementById("username").value;
//     var password = document.getElementById("password").value;

//     // Check if username or password fields are empty
//     if (!username || !password) {
//         alert("Please enter both username and password.");
//         return;
//     }

//     var loginData = {
//         username: username,
//         pass: password // Assuming your backend expects 'pass' for the password field
//     };

//     fetch("/login", {
//         method: "POST",
//         body: JSON.stringify(loginData),
//         headers: {
//             "Content-Type": "application/json; charset=UTF-8"
//         }
//     })
//     .then(response => {
//         if (response.ok) {
//             // Redirect to user home page on successful login
//             window.location.href = "./userFrontend/userHome.html";
//         } else {
//             // Parse the error response and handle accordingly
//             return response.json().then(data => {
//                 if (data && data.message) {
//                     throw new Error(data.message);
//                 } else {
//                     throw new Error("Failed to login. Please try again later.");
//                 }
//             });
//         }
//     })
//     .catch(error => {
//         alert("Error: " + error.message);
//     });
// }



function login(event) {
    // Prevent default form submission if this function is used as an event handler
    if (event) {
        event.preventDefault();
    }

    // Get the username and password from the input fields
    var _data = {
        username: document.getElementById("username").value,
        pass: document.getElementById("password").value
    };

    // Make the POST request with the login data
    fetch('/login', {
        method: "POST",
        body: JSON.stringify(_data),
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
    .then(response => {
        if (response.ok) {
            // Redirect to userHome.html if login is successful
            window.location.href = "./userFrontend/userHome.html";
        } else {
            // Parse the JSON response for error messages
            return response.json().then(err => { throw new Error(err.message); });
        }
    })
    .catch(e => {
        // Display appropriate error messages
        if (e.message === "Unauthorized") {
            alert("Unauthorized. Credentials do not match!");
        } else {
            alert("Error: " + e.message);
        }
    });
}

// Example usage: attach the login function to a form's submit event
document.getElementById("loginForm").addEventListener("submit", login);
