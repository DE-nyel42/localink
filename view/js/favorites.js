// Function to render favorite catalogs
function renderFavoriteCatalogs(favoriteCatalogs) {
    const favoriteCatalogContainer = document.getElementById('favorite-catalogs');
    favoriteCatalogContainer.innerHTML = ''; // Clear previous favorite catalogs if any
    console.log("Favorite catalogs:", favoriteCatalogs); // Log the fetched favorite catalogs

    favoriteCatalogs.forEach(catalog => {
        if (catalog.isfavorite) {
            const card = document.createElement('div');
            card.classList.add('col-md-4', 'mb-3');
            card.innerHTML = `
                <div class="card">
                    <div class="card-img">
                        <img src="${catalog.image_url}" class="card-img-top" alt="Catalog Image">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">${catalog.title}</h5>
                        <p class="card-text">${catalog.description}</p>
                        <button class="btn btn-danger btn-remove-favorite" data-catalog-id="${catalog.id}">Remove from Favorites</button>
                    </div>
                </div>
            `;
            console.log("Appending catalog:", catalog); // Log the catalog being appended
            favoriteCatalogContainer.appendChild(card);
        }
    });

    // Add event listeners to remove buttons
    const removeButtons = document.querySelectorAll('.btn-remove-favorite');
    removeButtons.forEach(button => {
        button.addEventListener('click', function(event) {
            const catalogId = event.target.getAttribute('data-catalog-id');
            console.log("Removing catalog with ID:", catalogId); // Log the catalog ID being removed
            removeFromFavorites(catalogId);
        });
    });
}

// Function to remove catalog from favorites
function removeFromFavorites(catalogId) {
    fetch(`http://localhost:7070/catalog/${catalogId}`, {
        method: "DELETE",
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to remove catalog from favorites');
        }
        return response.json();
    })
    .then(json => {
        console.log('Success:', json);
        alert("Catalog removed from favorites");
        fetchFavoriteCatalogs(); // Refresh favorite catalog list
    })
    .catch(error => {
        console.error('Error:', error);
        alert("Failed to remove catalog from favorites. Please try again later.");
    });
}


// Function to fetch favorite catalogs
function fetchFavoriteCatalogs() {
    fetch('http://localhost:7070/catalogs')
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to fetch favorite catalogs');
            }
            return response.json();
        })
        .then(favoriteCatalogs => {
            renderFavoriteCatalogs(favoriteCatalogs);
        })
        .catch(error => {
            console.error('Error:', error);
            alert("Failed to fetch favorite catalogs");
        });
}

// Fetch favorite catalogs when the page loads
window.onload = function() {
    fetchFavoriteCatalogs();
};
