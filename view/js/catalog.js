// document.getElementById("image").addEventListener("change", function () {
//     const reader = new FileReader();
//     reader.onload = function (event) {
//         localStorage.setItem("picture", event.target.result);
//     };
//     reader.readAsDataURL(this.files[0]);
// });

// // Function to get form data for adding new catalog
// function getCatalogFormData() {
//     const image = localStorage.getItem("picture");
//     if (!image) {
//         console.error("Image data not found in localStorage.");
//         return null;
//     }
//     const data = {
//         id: parseInt(document.getElementById("id").value), // Get the ID value
//         title: document.getElementById("title").value,
//         description: document.getElementById("description").value,
//         image_url: image
//     };
//     return data;
// }

// // Function to add catalog
// function addCatalog(event) {
//     event.preventDefault();
//     const data = getCatalogFormData();
//     if (!data) {
//         console.error("Missing or invalid data.");
//         return;
//     }
//     fetch('http://localhost:7070/catalog', {
//         method: "POST",
//         body: JSON.stringify(data),
//         headers: { "Content-type": "application/json; charset=UTF-8" }
//     })
//     .then(response => {
//         if (!response.ok) {
//             throw new Error('Network response was not ok');
//         }
//         return response.json();
//     })
//     .then(json => {
//         console.log('Success:', json);
//         alert("Catalog created successfully");
//         fetchCatalogs(); // Refresh catalog list
//         document.getElementById("catalogForm").reset(); // Reset the form
//         localStorage.removeItem("picture"); // Clear stored image
//     })
//     .catch(error => {
//         console.error('Error:', error);
//         alert("Failed to create catalog");
//     });
// }

// // Function to fetch catalogs
// function fetchCatalogs() {
//     fetch('http://localhost:7070/catalogs')
//         .then(response => {
//             if (!response.ok) {
//                 throw new Error('Failed to fetch catalogs');
//             }
//             return response.json();
//         })
//         .then(catalogs => {
//             renderCatalogs(catalogs);
//         })
//         .catch(error => {
//             console.error('Error:', error);
//             alert("Failed to fetch catalogs");
//         });
// }

// // Function to add catalog to favorites
// function addToFavorites(catalogId) {
//     fetch(`http://localhost:7070/catalog/${catalogId}/favorite`, {
//         method: "PUT",
//         headers: { "Content-type": "application/json; charset=UTF-8" }
//     })
//     .then(response => {
//         if (!response.ok) {
//             throw new Error('Network response was not ok');
//         }
//         return response.json();
//     })
//     .then(json => {
//         console.log('Success:', json);
//         alert("Catalog added to favorites");
//         fetchCatalogs(); // Refresh catalog list
//     })
//     .catch(error => {
//         console.error('Error:', error);
//         alert("Failed to add catalog to favorites");
//     });
// }

// // Function to render catalogs
// function renderCatalogs(catalogs) {
//     const catalogContainer = document.getElementById('catalog-container');
//     catalogContainer.innerHTML = ''; // Clear previous catalog if any
//     catalogs.forEach(catalog => {
//         const card = document.createElement('div');
//         card.classList.add('col-md-4', 'mb-3');
//         card.innerHTML = `
//             <div class="card">
//                 <div class="card-img">
//                     <img src="${catalog.image_url}" class="card-img-top" alt="...">
//                 </div>
//                 <div class="card-body">
//                     <p class="card-title"><strong>Title:</strong> ${catalog.title}</p>
//                     <p class="card-text"><strong>Description:</strong> ${catalog.description}</p>
//                     <button class="btn-update mb-2" onclick="addToFavorites(${catalog.id})">Add to Favorites</button>
//                 </div>
//             </div>
//         `;
//         catalogContainer.appendChild(card);
//     });
// }

// // fetching of catalogs when the page loads
// window.onload = fetchCatalogs;

// // Attach addCatalog function to form submit event
// document.getElementById('catalogForm').addEventListener('submit', addCatalog);

document.getElementById("image").addEventListener("change", function () {
    const reader = new FileReader();
    reader.onload = function (event) {
        localStorage.setItem("picture", event.target.result);
    };
    reader.readAsDataURL(this.files[0]);
});

// Function to get form data for adding new catalog
function getCatalogFormData() {
    const image = localStorage.getItem("picture");
    if (!image) {
        console.error("Image data not found in localStorage.");
        return null;
    }
    const data = {
        id: parseInt(document.getElementById("id").value), // Get the ID value
        title: document.getElementById("title").value,
        description: document.getElementById("description").value,
        image_url: image
    };
    return data;
}

// Function to add catalog
function addCatalog(event) {
    event.preventDefault();
    const data = getCatalogFormData();
    if (!data) {
        console.error("Missing or invalid data.");
        return;
    }
    fetch('http://localhost:7070/catalog', {
        method: "POST",
        body: JSON.stringify(data),
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(json => {
        console.log('Success:', json);
        alert("Catalog created successfully");
        fetchCatalogs(); // Refresh catalog list
        document.getElementById("catalogForm").reset(); // Reset the form
        localStorage.removeItem("picture"); // Clear stored image
    })
    .catch(error => {
        console.error('Error:', error);
        alert("Failed to create catalog");
    });
}

// Function to fetch catalogs
function fetchCatalogs() {
    fetch('http://localhost:7070/catalogs')
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to fetch catalogs');
            }
            return response.json();
        })
        .then(catalogs => {
            renderCatalogs(catalogs);
        })
        .catch(error => {
            console.error('Error:', error);
            alert("Failed to fetch catalogs");
        });
}

// Function to add catalog to favorites
function addToFavorites(catalogId) {
    fetch(`http://localhost:7070/catalog/${catalogId}/favorite`, {
        method: "PUT",
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(json => {
        console.log('Success:', json);
        alert("Catalog added to favorites");
        fetchCatalogs(); // Refresh catalog list
    })
    .catch(error => {
        console.error('Error:', error);
        alert("Failed to add catalog to favorites");
    });
}

// Function to render catalogs
function renderCatalogs(catalogs) {
    const catalogContainer = document.getElementById('catalog-container');
    catalogContainer.innerHTML = ''; // Clear previous catalog if any
    catalogs.forEach(catalog => {
        const card = document.createElement('div');
        card.classList.add('col-md-4', 'mb-3');
        card.innerHTML = `
            <div class="card">
                <div class="card-img">
                    <img src="${catalog.image_url}" class="card-img-top" alt="...">
                </div>
                <div class="card-body">
                    <p class="card-title"><strong>Title:</strong> ${catalog.title}</p>
                    <div class="d-flex flex-row justify-content-between">
                        <p class="card-text"><strong>Description:</strong> ${catalog.description}</p>
                        <p class="favorite-status">Favorite: ${catalog.isfavorite ? 'Yes' : 'No'}</p>
                    </div>
                     <button class="btn-update mb-2" onclick="addToFavorites(${catalog.id})">Add to Favorites</button>
                    </div>
                    </div>
                    `;
                    catalogContainer.appendChild(card);
                });
            }
            

// fetching of catalogs when the page loads
window.onload = fetchCatalogs;

// Attach addCatalog function to form submit event
document.getElementById('catalogForm').addEventListener('submit', addCatalog);
