package model

import (
	"log"
	"myapp/dataStore/postgres"
)

type Catalog struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	ImageURL    string `json:"image_url"`
	IsFavorite  bool   `json:"isfavorite"`
}

func CreateCatalog(catalog Catalog) (int, error) {
	query := `INSERT INTO catalog (id, title, description, image_url, isfavorite) 
              VALUES ($1, $2, $3, $4, $5) RETURNING id`

	var id int
	err := postgres.Db.QueryRow(query, catalog.ID, catalog.Title, catalog.Description, catalog.ImageURL, false).Scan(&id)
	if err != nil {
		log.Println("Error inserting catalog:", err)
		return 0, err
	}

	return id, nil
}

func GetAllCatalogs() ([]Catalog, error) {
	query := `SELECT id, title, description, image_url, isfavorite FROM catalog`

	rows, err := postgres.Db.Query(query)
	if err != nil {
		log.Println("Error querying catalogs:", err)
		return nil, err
	}
	defer rows.Close()

	var catalogs []Catalog
	for rows.Next() {
		var catalog Catalog
		if err := rows.Scan(&catalog.ID, &catalog.Title, &catalog.Description, &catalog.ImageURL, &catalog.IsFavorite); err != nil {
			log.Println("Error scanning catalog row:", err)
			return nil, err
		}
		catalogs = append(catalogs, catalog)
	}
	if err := rows.Err(); err != nil {
		log.Println("Error iterating over catalog rows:", err)
		return nil, err
	}

	return catalogs, nil
}

func UpdateCatalogFavorite(id int, isFavorite bool) error {
	query := `UPDATE catalog SET isfavorite = $1 WHERE id = $2`

	_, err := postgres.Db.Exec(query, isFavorite, id)
	if err != nil {
		log.Println("Error updating catalog favorite status:", err)
		return err
	}

	return nil
}

func DeleteCatalog(id int) error {
	query := "DELETE FROM catalog WHERE id = $1"

	_, err := postgres.Db.Exec(query, id)
	if err != nil {
		log.Println("Error deleting catalog:", err)
		return err
	}

	return nil
}
