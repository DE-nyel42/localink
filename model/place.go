package model

import (
	"log"
	"myapp/dataStore/postgres"
)

type Place struct {
	ID        int64   `json:"id"`
	Name      string  `json:"name"`
	Category  string  `json:"category"`
	Dzongkhag string  `json:"dzongkhag"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

const (
	queryGetPlace     = "SELECT id, name, category, dzongkhag, latitude, longitude FROM places WHERE id = $1;"
	queryUpdatePlace  = "UPDATE places SET name=$1, category=$2, dzongkhag=$3, latitude=$4, longitude=$5 WHERE id=$6 RETURNING id;"
	queryDeletePlace  = "DELETE FROM places WHERE id = $1 RETURNING id;"
	queryGetAllPlaces = "SELECT id, name, category, dzongkhag, latitude, longitude FROM places;"
	queryInsertPlace  = "INSERT INTO places (name, category, dzongkhag, latitude, longitude) VALUES ($1, $2, $3, $4, $5) RETURNING id;"
)

func (p *Place) Create() error {
	err := postgres.Db.QueryRow(queryInsertPlace, p.Name, p.Category, p.Dzongkhag, p.Latitude, p.Longitude).Scan(&p.ID)
	if err != nil {
		log.Printf("Error creating place: %v", err)
		return err
	}
	log.Printf("Place created with ID: %d", p.ID)
	return nil
}

func (p *Place) Read() error {
	err := postgres.Db.QueryRow(queryGetPlace, p.ID).Scan(&p.ID, &p.Name, &p.Category, &p.Dzongkhag, &p.Latitude, &p.Longitude)
	if err != nil {
		log.Printf("Error reading place: %v", err)
	}
	return err
}

func (p *Place) Update() error {
	result, err := postgres.Db.Exec(queryUpdatePlace, p.Name, p.Category, p.Dzongkhag, p.Latitude, p.Longitude, p.ID)
	if err != nil {
		log.Printf("Error updating place: %v", err)
		return err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		log.Printf("Error getting rows affected: %v", err)
		return err
	}
	if rowsAffected == 0 {
		log.Printf("No place updated with ID: %d", p.ID)
	}
	return nil
}

func (p *Place) Delete() error {
	result, err := postgres.Db.Exec(queryDeletePlace, p.ID)
	if err != nil {
		log.Printf("Error deleting place: %v", err)
		return err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		log.Printf("Error getting rows affected: %v", err)
		return err
	}
	if rowsAffected == 0 {
		log.Printf("No place deleted with ID: %d", p.ID)
	}
	return nil
}

func GetAllPlaces() ([]Place, error) {
	rows, err := postgres.Db.Query(queryGetAllPlaces)
	if err != nil {
		log.Printf("Error getting all places: %v", err)
		return nil, err
	}
	defer rows.Close()

	var places []Place
	for rows.Next() {
		var place Place
		err := rows.Scan(&place.ID, &place.Name, &place.Category, &place.Dzongkhag, &place.Latitude, &place.Longitude)
		if err != nil {
			log.Printf("Error scanning place: %v", err)
			return nil, err
		}
		places = append(places, place)
	}
	return places, nil
}
