package model

import (
	"database/sql"
	"fmt"
	"myapp/dataStore/postgres"
)

type Review struct {
	ID     int    `json:"id"`
	Rating int    `json:"rating"`
	Review string `json:"review"`
}

func CreateReview(review Review) error {
	query := `INSERT INTO reviews (rating, review) VALUES ($1, $2)`
	_, err := postgres.Db.Exec(query, review.Rating, review.Review)
	if err != nil {
		return fmt.Errorf("CreateReview: %v", err)
	}
	return nil
}
func GetReviews() ([]Review, error) {
	query := `SELECT id, rating, review FROM reviews`
	rows, err := postgres.Db.Query(query)
	if err != nil {
		return nil, fmt.Errorf("GetReviews: %v", err)
	}
	defer rows.Close()

	var reviews []Review
	for rows.Next() {
		var review Review
		if err := rows.Scan(&review.ID, &review.Rating, &review.Review); err != nil {
			return nil, fmt.Errorf("GetReviews: %v", err)
		}
		reviews = append(reviews, review)
	}

	return reviews, nil
}

func GetReview(id int) (Review, error) {
	query := `SELECT id, rating, review FROM reviews WHERE id = $1`
	row := postgres.Db.QueryRow(query, id)

	var review Review
	err := row.Scan(&review.ID, &review.Rating, &review.Review)
	if err == sql.ErrNoRows {
		return review, fmt.Errorf("GetReview: no review found with id %d", id)
	} else if err != nil {
		return review, fmt.Errorf("GetReview: %v", err)
	}

	return review, nil
}

func DeleteReview(id int) error {
	query := `DELETE FROM reviews WHERE id = $1`
	_, err := postgres.Db.Exec(query, id)
	if err != nil {
		return fmt.Errorf("DeleteReview: %v", err)
	}
	return nil
}
