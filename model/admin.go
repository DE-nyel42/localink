package model

import (
	"myapp/dataStore/postgres"

	"golang.org/x/crypto/bcrypt"
)

type Admin struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Pass     string `json:"pass"`
}

// query strings
const (
	queryInsertAdmin = "INSERT INTO admin(username,email,pass) VALUES($1, $2, $3) RETURNING username;"
	queryGetAdmin    = "SELECT email, pass FROM admin WHERE username=$1;"
	queryUpdateAdmin = "UPDATE admin SET username=$1, pass=$2, email=$3 WHERE username=$4;"
	queryGetByEmail  = "SELECT username, email, pass FROM admin WHERE email=$1;"
)

func (adm *Admin) Create() error {
	// Hash the password before saving
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(adm.Pass), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	adm.Pass = string(hashedPassword)

	row := postgres.Db.QueryRow(queryInsertAdmin, adm.Username, adm.Email, adm.Pass)
	err = row.Scan(&adm.Username)
	return err
}

func (adm *Admin) Get() error {
	var storedPassword string
	err := postgres.Db.QueryRow(queryGetAdmin, adm.Username).Scan(&adm.Email, &storedPassword)
	if err != nil {
		return err
	}

	// Compare the stored hashed password with the provided password
	err = bcrypt.CompareHashAndPassword([]byte(storedPassword), []byte(adm.Pass))
	if err != nil {
		return err
	}

	adm.Pass = storedPassword
	return nil
}

func (adm *Admin) GetByEmail(email string) error {
	return postgres.Db.QueryRow(queryGetByEmail, email).Scan(&adm.Username, &adm.Email, &adm.Pass)
}

func (adm *Admin) Update(username string) error {
	// Hash the password before updating
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(adm.Pass), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	adm.Pass = string(hashedPassword)
	_, err = postgres.Db.Exec(queryUpdateAdmin, adm.Username, adm.Pass, adm.Email, username)
	return err
}

// GetAllAdminProfiles retrieves profiles of all admins
func GetAllAdminProfiles() ([]Admin, error) {
	// Query the database to retrieve all admin profiles
	rows, err := postgres.Db.Query("SELECT username, email FROM admin")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var profiles []Admin
	for rows.Next() {
		var admin Admin
		if err := rows.Scan(&admin.Username, &admin.Email); err != nil {
			return nil, err
		}
		profiles = append(profiles, admin)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return profiles, nil
}

// DeleteAdminByEmail deletes an admin profile by email
func DeleteAdminByEmail(email string) error {
	// Execute the DELETE query to delete the admin profile by email
	_, err := postgres.Db.Exec("DELETE FROM admin WHERE email = $1", email)
	return err
}
