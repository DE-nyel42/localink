module myapp

go 1.22.0

require github.com/lib/pq v1.10.9 // indirec

require github.com/gorilla/mux v1.8.1 // indirecs

require golang.org/x/crypto v0.23.0 // indire

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

//require github.com/jmoiron/sqlx v1.4.0 // indirect
