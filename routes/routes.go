package routes

import (
	"log"
	"myapp/controller"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

func InitializeRoutes() {
	var port = 7070
	router := mux.NewRouter()
	//Admin
	router.HandleFunc("/signup", controller.Signup).Methods("POST")
	router.HandleFunc("/login", controller.Login).Methods("POST")
	router.HandleFunc("/logout", controller.Logout).Methods("POST")

	router.HandleFunc("/catalogs", controller.GetAllCatalogsHandler).Methods("GET")
	router.HandleFunc("/catalog", controller.AddCatalog).Methods("POST")
	router.HandleFunc("/catalog/{id}/favorite", controller.AddToFavoritesHandler).Methods("PUT")
	router.HandleFunc("/catalog/{id}", controller.DeleteCatalogHandler).Methods("DELETE")

	router.HandleFunc("/places", controller.GetPlaces).Methods("GET")
	router.HandleFunc("/places", controller.CreatePlace).Methods("POST")

	// Profile update
	router.HandleFunc("/update/{email}", controller.UpdateProfile).Methods("PUT")
	router.HandleFunc("/profile/{email}", controller.GetProfile).Methods("GET")
	router.HandleFunc("/profiles", controller.GetAllProfiles).Methods("GET")
	router.HandleFunc("/profile/{email}", controller.DeleteProfile).Methods("DELETE")
	router.HandleFunc("/profile/{username}", controller.UpdateProfile).Methods("PUT")
	// Review
	router.HandleFunc("/reviews", controller.GetReviews).Methods("GET")
	router.HandleFunc("/review/{id}", controller.GetReview).Methods("GET")
	router.HandleFunc("/review/create", controller.CreateReview).Methods("POST")
	router.HandleFunc("/review/{id}", controller.DeleteReview).Methods("DELETE")

	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)

	log.Println("application running on port http://localhost:7070", +port)
	log.Fatal(http.ListenAndServe(":7070", router)) //fatal prints out the error

}
