package controller

import (
	"bytes"
	"encoding/json"
	"myapp/model"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetPlaces(t *testing.T) {
	req, err := http.NewRequest("GET", "/places", nil)
	assert.NoError(t, err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(GetPlaces)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)

	var places []model.Place
	err = json.Unmarshal(rr.Body.Bytes(), &places)
	assert.NoError(t, err)
	// Add assertions as needed for the response body
}

func TestCreatePlace(t *testing.T) {
	place := model.Place{
		Name:      "Trongsa Dzong",
		Category:  "Dzong",
		Dzongkhag: "Dzongkhag",
		Latitude:  27.12345,
		Longitude: 89.12345,
	}

	placeJSON, err := json.Marshal(place)
	assert.NoError(t, err)

	req, err := http.NewRequest("POST", "/places", bytes.NewBuffer(placeJSON))
	assert.NoError(t, err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(CreatePlace)
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)

	var createdPlace model.Place
	err = json.Unmarshal(rr.Body.Bytes(), &createdPlace)
	assert.NoError(t, err)
	// Add assertions as needed for the created place
}
