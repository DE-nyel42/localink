package controller

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestSignup(t *testing.T) {
	// Create a sample admin JSON payload
	adminJSON := `{"username": "testuser", "email": "test@example.com", "pass": "password123"}`

	// Create a request with the sample admin JSON payload
	req, err := http.NewRequest("POST", "/signup", strings.NewReader(adminJSON))
	assert.NoError(t, err)

	// Create a ResponseRecorder to record the response
	rr := httptest.NewRecorder()

	// Call the Signup handler function directly
	Signup(rr, req)

	// Check the status code of the response
	assert.Equal(t, http.StatusCreated, rr.Code)

	// Check the response body
	expectedBody := `{"status":"Admin added"}`
	assert.Equal(t, expectedBody, rr.Body.String())
}

func TestLogin(t *testing.T) {
	// Create a sample admin JSON payload
	adminJSON := `{"username": "Denyel", "pass": "denyel"}`

	// Create a request with the sample admin JSON payload
	req, err := http.NewRequest("POST", "/login", strings.NewReader(adminJSON))
	assert.NoError(t, err)

	// Create a ResponseRecorder to record the response
	rr := httptest.NewRecorder()

	// Call the Login handler function directly
	Login(rr, req)

	// Check the status code of the response
	assert.Equal(t, http.StatusOK, rr.Code)

	// Check the response body
	expectedBody := `{"message":"Success"}`
	assert.Equal(t, expectedBody, rr.Body.String())

	// Check if a cookie named "my-cookie" is set in the response
	cookie := rr.Result().Cookies()[0]
	assert.Equal(t, "my-cookie", cookie.Name)
	assert.NotEmpty(t, cookie.Value)
	assert.True(t, cookie.Secure)
	assert.WithinDuration(t, cookie.Expires, cookie.Expires.Add(30*time.Minute), time.Second)
}
