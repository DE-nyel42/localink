package controller

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestAddCatalog(t *testing.T) {
	// Create a sample catalog JSON payload
	catalogJSON := `{"id": 1, "title": "Sample Catalog", "description": "Sample Description", "image_url": "sample.jpg", "isfavorite": false}`

	// Create a request with the sample catalog JSON payload
	req, err := http.NewRequest("POST", "/addCatalog", bytes.NewBuffer([]byte(catalogJSON)))
	assert.NoError(t, err)

	// Create a ResponseRecorder to record the response
	rr := httptest.NewRecorder()

	// Call the AddCatalog handler function directly
	AddCatalog(rr, req)

	// Check the status code of the response
	assert.Equal(t, http.StatusCreated, rr.Code)

	// Check the response body
	var response map[string]int
	err = json.Unmarshal(rr.Body.Bytes(), &response)
	assert.NoError(t, err)
	assert.Equal(t, 1, response["id"])
}

func TestGetAllCatalogsHandler(t *testing.T) {
	// Create a request for fetching all catalogs
	req, err := http.NewRequest("GET", "/getAllCatalogs", nil)
	assert.NoError(t, err)

	// Create a ResponseRecorder to record the response
	rr := httptest.NewRecorder()

	// Call the GetAllCatalogsHandler handler function directly
	GetAllCatalogsHandler(rr, req)

	// Check the status code of the response
	assert.Equal(t, http.StatusOK, rr.Code)

	// TODO: Validate response body if needed
}

func TestAddToFavoritesHandler(t *testing.T) {
	// Create a request for adding catalog to favorites
	req, err := http.NewRequest("POST", "/addToFavorites/{id}", nil)
	assert.NoError(t, err)

	// Set a sample catalog ID in request parameters
	req = mux.SetURLVars(req, map[string]string{"id": "2"})

	// Create a ResponseRecorder to record the response
	rr := httptest.NewRecorder()

	// Call the AddToFavoritesHandler handler function directly
	AddToFavoritesHandler(rr, req)

	// Check the status code of the response
	assert.Equal(t, http.StatusOK, rr.Code)

	// TODO: Validate response body if needed
}

func TestDeleteCatalogHandler(t *testing.T) {
	// Create a request for deleting catalog
	req, err := http.NewRequest("DELETE", "/deleteCatalog/{id}", nil)
	assert.NoError(t, err)

	// Set a sample catalog ID in request parameters
	req = mux.SetURLVars(req, map[string]string{"id": "1"})

	// Create a ResponseRecorder to record the response
	rr := httptest.NewRecorder()

	// Call the DeleteCatalogHandler handler function directly
	DeleteCatalogHandler(rr, req)

	// Check the status code of the response
	assert.Equal(t, http.StatusOK, rr.Code)

	// TODO: Validate response body if needed
}
