package controller

import (
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

func Signup(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&admin); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid JSON body")
		return
	}
	defer r.Body.Close()

	saveErr := admin.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}

	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "Admin added"})
}

func Login(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin
	err := json.NewDecoder(r.Body).Decode(&admin)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid JSON body")
		return
	}
	defer r.Body.Close()

	getErr := admin.Get()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusUnauthorized, "Invalid credentials")
		return
	}

	cookie := http.Cookie{
		Name:    "my-cookie",
		Value:   "my-value",
		Expires: time.Now().Add(30 * time.Minute),
		Secure:  true,
	}
	http.SetCookie(w, &cookie)
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "Success"})
}

func Logout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:    "my-cookie",
		Expires: time.Now(),
	})
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "Cookie deleted"})
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) (string, bool) {
	cookie, err := r.Cookie("my-cookie")
	if err != nil {
		if err == http.ErrNoCookie {
			httpResp.RespondWithError(w, http.StatusUnauthorized, "Cookie not found")
			return "", false
		}
		httpResp.RespondWithError(w, http.StatusInternalServerError, "Internal server error")
		return "", false
	}
	return cookie.Value, true
}

func UpdateProfile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	username := vars["username"] // Extract username from URL path

	// Verify the cookie
	cookieEmail, valid := VerifyCookie(w, r)
	if !valid {
		return
	}

	// Fetch the current admin details from the database using the email
	var existingAdmin model.Admin
	if err := existingAdmin.GetByEmail(cookieEmail); err != nil {
		httpResp.RespondWithError(w, http.StatusUnauthorized, "Unauthorized access")
		return
	}

	// Validate the username against the one stored in the database
	if existingAdmin.Username != username {
		httpResp.RespondWithError(w, http.StatusUnauthorized, "Unauthorized access")
		return
	}

	var admin model.Admin
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&admin); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid JSON body")
		return
	}
	defer r.Body.Close()

	// Ensure that password is provided
	if admin.Pass == "" {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Password is required")
		return
	}

	// Compare the provided password with the one stored in the database
	err := bcrypt.CompareHashAndPassword([]byte(existingAdmin.Pass), []byte(admin.Pass))
	if err != nil {
		httpResp.RespondWithError(w, http.StatusUnauthorized, "Invalid credentials")
		return
	}

	// Update the email if password verification succeeds
	existingAdmin.Email = admin.Email // Update the email with the new one

	if err := existingAdmin.Update(username); err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, "Error updating profile")
		return
	}

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "Profile updated successfully"})
}

func GetProfile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	email := vars["email"]

	// Verify the cookie
	cookieEmail, valid := VerifyCookie(w, r)
	if !valid {
		return
	}

	// Check if the email in the cookie matches the email in the URL
	if cookieEmail != email {
		httpResp.RespondWithError(w, http.StatusUnauthorized, "Unauthorized access")
		return
	}

	var admin model.Admin
	if err := admin.GetByEmail(email); err != nil {
		httpResp.RespondWithError(w, http.StatusNotFound, "User not found")
		return
	}

	httpResp.RespondWithJSON(w, http.StatusOK, admin)
}

// GetAllProfiles retrieves profiles of all users
func GetAllProfiles(w http.ResponseWriter, r *http.Request) {
	// Implement authentication if needed
	// For example, verify admin privileges

	// Retrieve all user profiles from the database
	profiles, err := model.GetAllAdminProfiles()
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, "Failed to retrieve user profiles")
		return
	}

	// Respond with the user profiles
	httpResp.RespondWithJSON(w, http.StatusOK, profiles)
}

// DeleteProfile deletes a user profile by email
func DeleteProfile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	email := vars["email"]

	// Verify the cookie or implement authentication if needed

	// Delete the user profile by email
	err := model.DeleteAdminByEmail(email)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, "Failed to delete user profile")
		return
	}

	// Respond with success message
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "User profile deleted successfully"})
}
