package controller

import (
	"encoding/json"
	"myapp/model"
	"net/http"
)

func GetPlaces(w http.ResponseWriter, r *http.Request) {
	places, err := model.GetAllPlaces()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(places)
}

func CreatePlace(w http.ResponseWriter, r *http.Request) {
	var place model.Place
	if err := json.NewDecoder(r.Body).Decode(&place); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := place.Create(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(place)
}

