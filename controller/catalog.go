package controller

import (
	"encoding/json"
	"log"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddCatalog(w http.ResponseWriter, r *http.Request) {
	var catalog model.Catalog
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&catalog); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()

	id, err := model.CreateCatalog(catalog)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, "failed to create catalog")
		return
	}

	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]int{"id": id})
}

func GetAllCatalogsHandler(w http.ResponseWriter, r *http.Request) {
	// fmt.Println("hello")
	catalogs, err := model.GetAllCatalogs()
	if err != nil {
		http.Error(w, "Failed to fetch catalogs", http.StatusInternalServerError)
		return
	}

	jsonResponse(w, catalogs)
}

func jsonResponse(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		log.Println("Error encoding JSON response:", err)
		http.Error(w, "Failed to encode JSON response", http.StatusInternalServerError)
		return
	}
}

func AddToFavoritesHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid catalog ID")
		return
	}

	err = model.UpdateCatalogFavorite(id, true)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, "Failed to update catalog favorite status")
		return
	}

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "Catalog added to favorites"})
}

func DeleteCatalogHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid catalog ID")
		return
	}

	err = model.DeleteCatalog(id)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, "Failed to delete catalog")
		return
	}

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "Catalog deleted successfully"})
}
